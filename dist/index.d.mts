import * as _nestjs_common from '@nestjs/common';
import { ExceptionFilter, ArgumentsHost, NestInterceptor, ExecutionContext, CallHandler, PipeTransform, ArgumentMetadata, CanActivate } from '@nestjs/common';
import * as mongoose from 'mongoose';
import { Document } from 'mongoose';
import { Cache } from 'cache-manager';
import { Request as Request$1 } from 'express';
import * as rxjs from 'rxjs';
import { Observable } from 'rxjs';
import * as _nestjs_passport from '@nestjs/passport';
import * as joi from 'joi';
import { Reflector } from '@nestjs/core';

declare const mongoDbUrl: string;
declare const MongoDbConnection: _nestjs_common.DynamicModule;

interface IHeaders {
    languageKey?: string;
    priceKey?: string;
    storeView?: string;
}

interface IUser {
    account: string;
    privileges: any[] | null;
}

interface IRequest extends Request {
    user: IUser;
}

interface IParamsId {
    id: string;
}

interface IPagination {
    limit: number;
    skip: number;
    total: boolean;
    needPagination?: boolean;
}

interface IError {
    code: number;
    message: string;
}

interface IOtp {
    email: string;
    code: number;
    status: string;
    account: string;
}

interface BaseMongoInterface<V, T> {
    create({ doc, options }: {
        doc: V;
        options?: mongoose.SaveOptions;
    }): any;
    countDocuments({ filter, options, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: any;
    }): any;
    aggregate({ pipeline, options, }: {
        pipeline?: mongoose.PipelineStage[];
        options?: mongoose.AggregateOptions;
    }): any;
    find({ filter, projection, options, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
    }): any;
    findOne({ filter, projection, options, error, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    findById({ _id, projection, options, error, }: {
        _id: any;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    findOneAndUpdate({ filter, update, options, error, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    findOneAndDelete({ filter, options, error, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    findByIdAndUpdate({ _id, update, options, error, }: {
        _id: any;
        update?: mongoose.UpdateQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    findByIdAndDelete({ _id, options, error, }: {
        _id: mongoose.Types.ObjectId;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): any;
    updateMany({ filter, update, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateWithAggregationPipeline | mongoose.UpdateQuery<T>;
        options?: any;
        error: IError;
        throwError?: boolean;
    }): any;
    updateOne({ filter, update, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateWithAggregationPipeline | mongoose.UpdateQuery<T>;
        options?: mongoose.MongooseQueryOptions<T>;
        error: IError;
        throwError?: boolean;
    }): any;
    insertMany({ docs, options, }: {
        docs: V[];
        options?: mongoose.InsertManyOptions & {
            lean: true;
        };
    }): any;
    distinct({ field, filter, }: {
        field: string;
        filter?: mongoose.FilterQuery<T>;
    }): any;
    deleteMany({ filter, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: any;
        error: IError;
        throwError?: boolean;
    }): any;
    getSequenceId({ options }: {
        options?: mongoose.QueryOptions<T>;
    }): any;
    findAndCount({ filter, projection, options, total, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        total?: boolean;
    }): any;
}

declare abstract class BaseMongoRepository<V, T extends Document> implements BaseMongoInterface<V, T> {
    private readonly entityModel;
    constructor(entityModel: mongoose.Model<T>);
    create({ doc, options, needResult, }: {
        doc: V;
        options?: mongoose.SaveOptions;
        needResult?: boolean;
    }): Promise<mongoose.Require_id<T>['_id']>;
    countDocuments({ filter, options, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: any;
    }): Promise<number>;
    aggregate({ pipeline, options, }: {
        pipeline?: mongoose.PipelineStage[];
        options?: mongoose.AggregateOptions;
    }): Promise<any[]>;
    find({ filter, projection, options, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
    }): Promise<mongoose.Require_id<mongoose.FlattenMaps<T>>[]>;
    findOne({ filter, projection, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
        throwError?: boolean;
    }): Promise<T extends any[] ? mongoose.Require_id<mongoose.FlattenMaps<T>>[] : mongoose.Require_id<mongoose.FlattenMaps<T>>>;
    findById({ _id, projection, options, error, }: {
        _id: string | mongoose.Types.ObjectId;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): Promise<T extends any[] ? mongoose.Require_id<mongoose.FlattenMaps<T>>[] : mongoose.Require_id<mongoose.FlattenMaps<T>>>;
    findOneAndUpdate({ filter, update, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
        throwError?: boolean;
    }): Promise<T | null>;
    findOneAndDelete({ filter, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
        throwError?: boolean;
    }): Promise<T | null>;
    findByIdAndUpdate({ _id, update, options, error, }: {
        _id: string | mongoose.Types.ObjectId;
        update?: mongoose.UpdateQuery<T>;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): Promise<T | null>;
    findByIdAndDelete({ _id, options, error, }: {
        _id: mongoose.Types.ObjectId;
        options?: mongoose.QueryOptions<T>;
        error: IError;
    }): Promise<T | null>;
    updateMany({ filter, update, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateWithAggregationPipeline | mongoose.UpdateQuery<T>;
        options?: any;
        error: IError;
        throwError?: boolean;
    }): Promise<mongoose.UpdateWriteOpResult>;
    updateOne({ filter, update, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        update?: mongoose.UpdateWithAggregationPipeline | mongoose.UpdateQuery<T>;
        options?: mongoose.MongooseQueryOptions<T>;
        error: IError;
        throwError?: boolean;
    }): Promise<mongoose.UpdateWriteOpResult>;
    insertMany({ docs, options, }: {
        docs: V[];
        options?: mongoose.InsertManyOptions & {
            lean: true;
        };
    }): Promise<mongoose.Require_id<V>[]>;
    distinct({ field, filter, }: {
        field: string;
        filter?: mongoose.FilterQuery<T>;
    }): Promise<(string extends infer T_1 ? T_1 extends string ? T_1 extends keyof T ? mongoose.Unpacked<T[T_1]> : unknown : never : never)[]>;
    deleteMany({ filter, options, error, throwError, }: {
        filter?: mongoose.FilterQuery<T>;
        options?: any;
        error: IError;
        throwError?: boolean;
    }): Promise<mongoose.mongo.DeleteResult>;
    getSequenceId({ options, tableHaveSequenceId, needUpdate, updateFunction, }: {
        options?: mongoose.QueryOptions<T>;
        tableHaveSequenceId: true;
        needUpdate?: boolean;
        updateFunction?: Function;
    }): Promise<number>;
    findAndCount({ filter, projection, options, total, }: {
        filter?: mongoose.FilterQuery<T>;
        projection?: mongoose.ProjectionType<T>;
        options?: mongoose.QueryOptions<T>;
        total?: boolean;
    }): Promise<{
        items: T[];
        totalRecords?: number;
    }>;
}

declare const mongoDbConfig: {
    dbUrl: string;
    dbName: string;
    dbHost: string;
    dbPort: string;
    dbUser: string;
    dbPassword: string;
};

type LocationDocument = Location & Document;
declare class Location {
    latitude: number;
    longitude: number;
}
declare const LocationSchema: mongoose.Schema<Location, mongoose.Model<Location, any, any, any, Document<unknown, any, Location> & Location & {
    _id: mongoose.Types.ObjectId;
}, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, Location, Document<unknown, {}, mongoose.FlatRecord<Location>> & mongoose.FlatRecord<Location> & {
    _id: mongoose.Types.ObjectId;
}>;

type LocalizableStringDocument = LocalizableString & Document;
declare class LocalizableString {
    en: string;
    ar: string;
}
declare const LocalizableStringSchema: mongoose.Schema<LocalizableString, mongoose.Model<LocalizableString, any, any, any, Document<unknown, any, LocalizableString> & LocalizableString & {
    _id: mongoose.Types.ObjectId;
}, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, LocalizableString, Document<unknown, {}, mongoose.FlatRecord<LocalizableString>> & mongoose.FlatRecord<LocalizableString> & {
    _id: mongoose.Types.ObjectId;
}>;

type PriceDocument = Price & Document;
declare class Price {
    USD: number;
    AED: number;
}
declare const PriceSchema: mongoose.Schema<Price, mongoose.Model<Price, any, any, any, Document<unknown, any, Price> & Price & {
    _id: mongoose.Types.ObjectId;
}, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, Price, Document<unknown, {}, mongoose.FlatRecord<Price>> & mongoose.FlatRecord<Price> & {
    _id: mongoose.Types.ObjectId;
}>;

type LocalFileDocument = LocalFile & Document;
declare class LocalFile {
    filename: string;
    mimetype: string;
    key: string;
    originalFilename: string;
    relativePath: string;
    extension: string;
}
declare const LocalFileSchema: mongoose.Schema<LocalFile, mongoose.Model<LocalFile, any, any, any, Document<unknown, any, LocalFile> & LocalFile & {
    _id: mongoose.Types.ObjectId;
}, any>, {}, {}, {}, {}, mongoose.DefaultSchemaOptions, LocalFile, Document<unknown, {}, mongoose.FlatRecord<LocalFile>> & mongoose.FlatRecord<LocalFile> & {
    _id: mongoose.Types.ObjectId;
}>;

type index$1_LocalFile = LocalFile;
declare const index$1_LocalFile: typeof LocalFile;
type index$1_LocalFileDocument = LocalFileDocument;
declare const index$1_LocalFileSchema: typeof LocalFileSchema;
type index$1_LocalizableString = LocalizableString;
declare const index$1_LocalizableString: typeof LocalizableString;
type index$1_LocalizableStringDocument = LocalizableStringDocument;
declare const index$1_LocalizableStringSchema: typeof LocalizableStringSchema;
type index$1_Location = Location;
declare const index$1_Location: typeof Location;
type index$1_LocationDocument = LocationDocument;
declare const index$1_LocationSchema: typeof LocationSchema;
type index$1_Price = Price;
declare const index$1_Price: typeof Price;
type index$1_PriceDocument = PriceDocument;
declare const index$1_PriceSchema: typeof PriceSchema;
declare namespace index$1 {
  export { index$1_LocalFile as LocalFile, type index$1_LocalFileDocument as LocalFileDocument, index$1_LocalFileSchema as LocalFileSchema, index$1_LocalizableString as LocalizableString, type index$1_LocalizableStringDocument as LocalizableStringDocument, index$1_LocalizableStringSchema as LocalizableStringSchema, index$1_Location as Location, type index$1_LocationDocument as LocationDocument, index$1_LocationSchema as LocationSchema, index$1_Price as Price, type index$1_PriceDocument as PriceDocument, index$1_PriceSchema as PriceSchema };
}

declare class AllHttpExceptionsFilter implements ExceptionFilter {
    private readonly logger;
    catch(exception: unknown, host: ArgumentsHost): void;
}

declare const winston: _nestjs_common.DynamicModule;

declare const RedisConnection: _nestjs_common.DynamicModule;

declare const JwtModule: _nestjs_common.DynamicModule;

declare class LocalizationService {
    price({ obj, priceKey }: {
        obj: any;
        priceKey: string;
    }): number;
    string({ obj, languageKey }: {
        obj: any;
        languageKey: string;
    }): string;
}

declare class FileUrlService {
    generate({ filename, storageFilePath, }: {
        filename: string;
        storageFilePath: string;
    }): string;
}

declare class RedisService {
    private cacheManager;
    constructor(cacheManager: Cache);
    get({ key }: {
        key: string;
    }): Promise<any>;
    set({ key, value, ttl, }: {
        key: string;
        value: unknown;
        ttl?: number;
    }): Promise<any>;
    del({ key }: {
        key: string;
    }): Promise<any>;
}

declare class CreatePrivilegeService {
    private static httpMethod;
    static create(method: keyof typeof CreatePrivilegeService.httpMethod, featureName: string): string;
}

declare const getSortFormat: (sortKey: string) => {
    [x: string]: number;
};

declare class RefreshTokenTTLInterceptor implements NestInterceptor {
    private readonly redisService;
    constructor(redisService: RedisService);
    intercept(context: ExecutionContext, next: CallHandler): Observable<any>;
}

declare const JwtStrategy_base: new (...args: any[]) => any;
declare class JwtStrategy extends JwtStrategy_base {
    private readonly redisService;
    constructor(redisService: RedisService);
    validate(req: Request$1, payload: any): Promise<any>;
}

declare const JwtAuthGuard_base: _nestjs_passport.Type<_nestjs_passport.IAuthGuard>;
declare class JwtAuthGuard extends JwtAuthGuard_base {
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | rxjs.Observable<boolean>;
    handleRequest(err: any, user: any, info: any): any;
}

declare const jwtConfig: {
    accessTokenSecret: string;
    expiresInJwt: string;
    issuer: string;
};

declare const passportStrategy: {
    local: string;
    localMobile: string;
    otp: string;
    jwt: string;
};

declare class JoiValidationPipe implements PipeTransform {
    private readonly schema;
    unknown: boolean;
    constructor(schema: joi.ObjectSchema, unknown?: boolean);
    transform(value: any, metadata?: ArgumentMetadata): any;
}

declare const localizableString: () => joi.ObjectSchema<any>;

declare const mongoId: () => joi.StringSchema<string>;

declare const password: () => joi.StringSchema<string>;

declare const stringNumber: () => joi.StringSchema<string>;

declare const languageObject: () => joi.ObjectSchema<any>;

declare const parseStringRule: (rule: string, valueRequired?: boolean) => joi.ObjectSchema<any> | joi.AlternativesSchema<any>;

declare const alpha: () => joi.StringSchema<string>;

declare const stringRole: () => joi.StringSchema<string>;

declare const index_alpha: typeof alpha;
declare const index_languageObject: typeof languageObject;
declare const index_localizableString: typeof localizableString;
declare const index_mongoId: typeof mongoId;
declare const index_parseStringRule: typeof parseStringRule;
declare const index_password: typeof password;
declare const index_stringNumber: typeof stringNumber;
declare const index_stringRole: typeof stringRole;
declare namespace index {
  export { index_alpha as alpha, index_languageObject as languageObject, index_localizableString as localizableString, index_mongoId as mongoId, index_parseStringRule as parseStringRule, index_password as password, index_stringNumber as stringNumber, index_stringRole as stringRole };
}

declare class PoliciesGuard implements CanActivate {
    private reflector;
    constructor(reflector: Reflector);
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean>;
}

declare const Policies: (keys: [string, ...string[]], values: string[]) => _nestjs_common.CustomDecorator<string>;

declare class UserTypesGuard implements CanActivate {
    private reflector;
    constructor(reflector: Reflector);
    canActivate(context: ExecutionContext): boolean | Promise<boolean> | Observable<boolean>;
}

declare const UserTypesMetadata: (values: string[]) => _nestjs_common.CustomDecorator<string>;

declare const paginationConstant: {
    total: boolean;
    needPagination: boolean;
    page: number;
    limit: {
        default: number;
        max: number;
    };
    sort: {
        _id: number;
    };
};

declare const joiPagination: (needPaginationValidValues: [boolean, ...boolean[]]) => {
    total: joi.BooleanSchema<boolean>;
    needPagination: joi.BooleanSchema<boolean>;
    page: joi.NumberSchema<number>;
    limit: joi.NumberSchema<number>;
};

declare const paginationParser: (reqQuery: any) => {
    pagination: any;
    criteria: any;
};

declare class PaginationRequest {
    total: boolean;
    needPagination: boolean;
    page: number;
    limit: number;
}

declare const serverConfig: {
    host: string;
    port: string;
    nodeEnv: string;
    apIVersion: string;
    globalPrefix: string;
    swaggerUsername: string;
    swaggerPassword: string;
    defaultLanguage: string;
    defaultPrice: string;
    mainLocalFileUploadDestination: string;
    storageUploadLostFile: string;
    fileUrlPrefix: string;
    fileSaveBasePath: string;
    serverUrl: string;
    otpValidTime: string;
    tokenTTL: number;
};

declare function AuthenticatedController({ controller, }: {
    controller: string;
}): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;
declare function CustomController({ guard, controller, }: {
    guard: CanActivate;
    controller: string;
}): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;

declare enum APIMETHODS {
    GET = "GET",
    POST = "POST",
    PATCH = "PATCH",
    PUT = "PUT",
    DELETE = "DELETE"
}

declare function AuthorizedApiMethod({ apiUrl, method, privilegeKeys, policyAccessMode, }: {
    apiUrl: string;
    method: APIMETHODS;
    privilegeKeys: [string, ...string[]];
    policyAccessMode?: string[];
}): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;

declare function UserType(...values: string[]): <TFunction extends Function, Y>(target: object | TFunction, propertyKey?: string | symbol, descriptor?: TypedPropertyDescriptor<Y>) => void;

declare const User: (...dataOrPipes: unknown[]) => ParameterDecorator;
declare const UserContent: (...dataOrPipes: (string | _nestjs_common.PipeTransform<any, any> | _nestjs_common.Type<_nestjs_common.PipeTransform<any, any>>)[]) => ParameterDecorator;

declare const Headers: (...dataOrPipes: unknown[]) => ParameterDecorator;
declare const HeadersContent: (...dataOrPipes: (string | _nestjs_common.PipeTransform<any, any> | _nestjs_common.Type<_nestjs_common.PipeTransform<any, any>>)[]) => ParameterDecorator;

declare class ResponseWrapper {
    data: any;
    constructor(data: any);
}

export { APIMETHODS, AllHttpExceptionsFilter, AuthenticatedController, AuthorizedApiMethod, type BaseMongoInterface, BaseMongoRepository, CreatePrivilegeService, CustomController, FileUrlService, Headers, HeadersContent, type IError, type IHeaders, type IOtp, type IPagination, type IParamsId, type IRequest, type IUser, JoiValidationPipe, JwtAuthGuard, JwtModule, JwtStrategy, LocalizationService, MongoDbConnection, PaginationRequest, Policies, PoliciesGuard, RedisConnection, RedisService, RefreshTokenTTLInterceptor, ResponseWrapper, User, UserContent, UserType, UserTypesGuard, UserTypesMetadata, getSortFormat, joiPagination, jwtConfig, mongoDbConfig, mongoDbUrl, index$1 as mongodbSchemas, paginationConstant, paginationParser, passportStrategy, serverConfig, index as validationSchemas, winston };
