var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __require = /* @__PURE__ */ ((x) => typeof require !== "undefined" ? require : typeof Proxy !== "undefined" ? new Proxy(x, {
  get: (a, b) => (typeof require !== "undefined" ? require : a)[b]
}) : x)(function(x) {
  if (typeof require !== "undefined")
    return require.apply(this, arguments);
  throw Error('Dynamic require of "' + x + '" is not supported');
});
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __decorateClass = (decorators, target, key, kind) => {
  var result = kind > 1 ? void 0 : kind ? __getOwnPropDesc(target, key) : target;
  for (var i = decorators.length - 1, decorator; i >= 0; i--)
    if (decorator = decorators[i])
      result = (kind ? decorator(target, key, result) : decorator(result)) || result;
  if (kind && result)
    __defProp(target, key, result);
  return result;
};
var __decorateParam = (index, decorator) => (target, key) => decorator(target, key, index);

// src/database/mongo/mongodb.connection.ts
import { MongooseModule } from "@nestjs/mongoose";

// src/database/mongo/db.config.ts
import * as dotenv from "dotenv";
dotenv.config();
var mongoDbConfig = {
  dbUrl: process.env.DB_URL,
  // Connection string for MongoDB
  dbName: process.env.DB_NAME,
  // Name of the database
  dbHost: process.env.DB_HOST,
  // Host where MongoDB is running
  dbPort: process.env.DB_PORT,
  // Port on which MongoDB is listening
  dbUser: process.env.DB_USER,
  // Username for MongoDB authentication
  dbPassword: process.env.DB_USER_PASSWORD
  // Password for MongoDB authentication
};

// src/database/mongo/mongodb.connection.ts
var mongoDbUrl = mongoDbConfig.dbUser && mongoDbConfig.dbPassword ? `mongodb://${mongoDbConfig.dbUser}:${mongoDbConfig.dbPassword}@${mongoDbConfig.dbHost}:${mongoDbConfig.dbPort}/${mongoDbConfig.dbName}` : `${mongoDbConfig.dbUrl}${mongoDbConfig.dbName}`;
var MongoDbConnection = MongooseModule.forRoot(mongoDbUrl, {
  dbName: mongoDbConfig.dbName,
  // Set the database name
  bufferCommands: false,
  // Disable buffering; requires connection to database server
  retryWrites: false,
  // Disable retry writes
  connectionFactory: (connection) => {
    connection.plugin(__require("mongoose-autopopulate"));
    connection.plugin((schema) => {
      schema.options.toJSON = {
        // Customize JSON output
        virtuals: true,
        // Include virtuals
        versionKey: false,
        // Remove version key
        transform(doc, ret) {
          if (ret.password)
            delete ret.password;
          ret.id = ret._id;
          delete ret._id;
        }
      };
      schema.options.toObject = {
        // Customize object output
        virtuals: true,
        // Include virtuals
        versionKey: false,
        // Remove version key
        transform(doc, ret) {
          if (ret.password)
            delete ret.password;
          delete ret._id;
        }
      };
    });
    return connection;
  }
});

// src/database/mongo/repository/index.ts
import { HttpException, HttpStatus } from "@nestjs/common";
var BaseMongoRepository = class {
  // Model representing the Mongoose model for entity operations
  constructor(entityModel) {
    this.entityModel = entityModel;
  }
  /**
   * Creates a new document in the database.
   * @param {object} params - Parameters including document to create and mongoose save options.
   * @returns The created document or its ID based on the needResult flag.
   */
  async create({
    doc,
    options,
    needResult = false
  }) {
    const result = await new this.entityModel(doc).save(options);
    return needResult ? result : result._id;
  }
  /**
   * Counts the documents that match the given filter.
   * @param {object} params - Filter criteria and options for counting.
   * @returns The count of documents that match the filter.
   */
  async countDocuments({
    filter,
    options
  }) {
    return await this.entityModel.countDocuments(filter, options);
  }
  /**
   * Executes an aggregation pipeline on the documents.
   * @param {object} params - Aggregation pipeline stages and options.
   * @returns The result of the aggregation operation.
   */
  async aggregate({
    pipeline,
    options
  }) {
    return await this.entityModel.aggregate(pipeline, options);
  }
  /**
   * Finds documents based on the specified filter, projection, and options.
   * @param {object} params - Criteria for filtering, projection, and query options.
   * @returns An array of documents that match the filter criteria.
   */
  async find({
    filter,
    projection,
    options
  }) {
    return await this.entityModel.find(filter, projection, {
      ...options,
      lean: true
    });
  }
  /**
   * Finds a single document based on the filter. Throws an error if not found when throwError is true.
   * @param {object} params - Filter, projection, options, and error handling configurations.
   * @returns A single document or null if not found (unless throwError is true).
   */
  async findOne({
    filter,
    projection,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.findOne(filter, projection, {
      ...options,
      lean: true
    });
    if (!result && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Finds a single document by its ID. Throws an error if not found.
   * @param {object} params - ID of the document to find, along with projection, options, and error handling configurations.
   * @returns The document found or throws an exception if not found.
   */
  async findById({
    _id,
    projection,
    options,
    error
  }) {
    const result = await this.entityModel.findById(_id, projection, {
      ...options,
      lean: true
    });
    if (!result) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Finds a single document based on the filter and updates it. Optionally throws an error if not found.
   * @param {object} params - Filter criteria, update data, query options, and error handling configurations.
   * @returns The document after updates or null if not found (unless throwError is true).
   */
  async findOneAndUpdate({
    filter,
    update,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.findOneAndUpdate(filter, update, {
      ...options,
      new: true
    });
    if (!result && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Finds a document by the provided filter and deletes it. Optionally throws an error if not found.
   * @param {object} params - Filter criteria, query options, and error handling configurations.
   * @returns The deleted document or null if no document was found (unless throwError is true).
   */
  async findOneAndDelete({
    filter,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.findOneAndDelete(filter, options);
    if (!result && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Updates a document identified by its ID with the given update data.
   * @param {object} params - ID of the document, update data, query options, and error handling configurations.
   * @returns The updated document or throws an exception if not found.
   */
  async findByIdAndUpdate({
    _id,
    update,
    options,
    error
  }) {
    const result = await this.entityModel.findByIdAndUpdate(_id, update, {
      ...options,
      new: true
    });
    if (!result) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Deletes a document by its ID.
   * @param {object} params - ID of the document, query options, and error handling configurations.
   * @returns The deleted document or throws an exception if not found.
   */
  async findByIdAndDelete({
    _id,
    options,
    error
  }) {
    const result = await this.entityModel.findByIdAndDelete(_id, options);
    if (!result) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Updates multiple documents that match the given filter with the provided update data.
   * @param {object} params - Filter criteria, update data, query options, and error handling configurations.
   * @returns The result of the update operation indicating success or failure.
   */
  async updateMany({
    filter,
    update,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.updateMany(filter, update, options);
    if (!result.modifiedCount && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Updates a single document that matches the given filter with the provided update data.
   * @param {object} params - Filter criteria, update data, query options, and error handling configurations.
   * @returns The result of the update operation indicating success or failure.
   */
  async updateOne({
    filter,
    update,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.updateOne(filter, update, options);
    if (!result.modifiedCount && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Inserts multiple documents into the database efficiently.
   * @param {object} params - Documents to insert and insertion options.
   * @returns An array of the inserted documents.
   */
  async insertMany({
    docs,
    options
  }) {
    return await this.entityModel.insertMany(docs, options);
  }
  /**
   * Retrieves distinct values for a specified field across a collection based on a filter.
   * @param {object} params - Field name and filter criteria.
   * @returns An array of distinct values.
   */
  async distinct({
    field,
    filter
  }) {
    return await this.entityModel.distinct(field, filter);
  }
  /**
   * Deletes multiple documents based on the provided filter.
   * @param {object} params - Filter criteria, query options, and error handling configurations.
   * @returns The result of the delete operation indicating success or failure.
   */
  async deleteMany({
    filter,
    options,
    error,
    throwError = true
  }) {
    const result = await this.entityModel.deleteMany(filter, options);
    if (!result.deletedCount && throwError) {
      throw new HttpException({ error }, HttpStatus.NOT_FOUND);
    }
    return result;
  }
  /**
   * Retrieves a unique sequence ID for new documents, often used for creating auto-incrementing fields.
   * @param {object} params - Options for the query and custom functions for updating the sequence ID.
   * @param {boolean} needUpdate - Whether to update the sequence ID in the database.
   * @param {Function} updateFunction - Function to calculate the new sequence ID based on the last one.
   * @returns The next sequence ID to be used.
   */
  async getSequenceId({
    options,
    tableHaveSequenceId,
    needUpdate = false,
    updateFunction = () => {
    }
  }) {
    const projection = "sequenceId";
    let sequenceId = 1;
    let lastItem = await this.entityModel.find({}, projection, {
      ...options,
      sort: { createdAt: -1 },
      limit: 1
    });
    if (needUpdate && lastItem.length) {
      lastItem[0].sequenceId = updateFunction(lastItem[0].sequenceId);
    }
    if (lastItem.length !== 0)
      sequenceId = +lastItem[0].sequenceId + 1;
    return sequenceId;
  }
  /**
   * Finds documents according to the filter and projection specified, and optionally counts the total number of documents matching the filter.
   * @param {object} params - Filter criteria, projection, query options, and whether a total count is needed.
   * @returns An object containing the documents found and, if requested, the total count of documents matching the filter.
   */
  async findAndCount({
    filter,
    projection,
    options,
    total = false
  }) {
    const queries = [];
    queries.push(
      this.entityModel.find(filter, projection, {
        ...options,
        lean: true
      })
    );
    if (total) {
      queries.push(this.entityModel.countDocuments(filter));
    }
    const [items, totalRecords = void 0] = await Promise.all(queries);
    return {
      items,
      totalRecords
    };
  }
};

// src/database/mongo/schemas/index.ts
var schemas_exports = {};
__export(schemas_exports, {
  LocalFile: () => LocalFile,
  LocalFileSchema: () => LocalFileSchema,
  LocalizableString: () => LocalizableString,
  LocalizableStringSchema: () => LocalizableStringSchema,
  Location: () => Location,
  LocationSchema: () => LocationSchema,
  Price: () => Price,
  PriceSchema: () => PriceSchema
});

// src/database/mongo/schemas/location.schema.ts
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
var Location = class {
};
// Latitude component of the location.
__decorateClass([
  Prop({ type: Number, required: true })
], Location.prototype, "latitude", 2);
// Longitude component of the location.
__decorateClass([
  Prop({ type: Number, required: true })
], Location.prototype, "longitude", 2);
Location = __decorateClass([
  Schema({ timestamps: false, _id: false })
], Location);
var LocationSchema = SchemaFactory.createForClass(Location);

// src/database/mongo/schemas/localizableString.schema.ts
import { Prop as Prop2, Schema as Schema2, SchemaFactory as SchemaFactory2 } from "@nestjs/mongoose";
var LocalizableString = class {
};
// Property to store English string values.
__decorateClass([
  Prop2({ type: String })
], LocalizableString.prototype, "en", 2);
// Property to store Arabic string values.
__decorateClass([
  Prop2({ type: String })
], LocalizableString.prototype, "ar", 2);
LocalizableString = __decorateClass([
  Schema2({ timestamps: false, _id: false })
], LocalizableString);
var LocalizableStringSchema = SchemaFactory2.createForClass(LocalizableString);

// src/database/mongo/schemas/price.schema.ts
import { Prop as Prop3, Schema as Schema3, SchemaFactory as SchemaFactory3 } from "@nestjs/mongoose";
var Price = class {
};
// Default price in US Dollars, initialized to 0.
__decorateClass([
  Prop3({ type: Number, default: 0 })
], Price.prototype, "USD", 2);
// Price in UAE Dirhams, mandatory field, initialized to 0.
__decorateClass([
  Prop3({ type: Number, default: 0, required: true })
], Price.prototype, "AED", 2);
Price = __decorateClass([
  Schema3({ timestamps: false, _id: false })
], Price);
var PriceSchema = SchemaFactory3.createForClass(Price);

// src/database/mongo/schemas/localFile.schema.ts
import { Prop as Prop4, Schema as Schema4, SchemaFactory as SchemaFactory4 } from "@nestjs/mongoose";
var LocalFile = class {
};
// Database schema definition for storing the filename.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "filename", 2);
// Mime type of the file, used to determine the file type.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "mimetype", 2);
// Unique key or identifier for the file in the storage system.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "key", 2);
// Original filename as uploaded by the user.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "originalFilename", 2);
// Relative path within the storage system where the file is stored.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "relativePath", 2);
// File extension, derived from the filename or mime type.
__decorateClass([
  Prop4({ type: String, required: true })
], LocalFile.prototype, "extension", 2);
LocalFile = __decorateClass([
  Schema4({ timestamps: true })
], LocalFile);
var LocalFileSchema = SchemaFactory4.createForClass(LocalFile);

// src/error-handlers/allHttpExceptionsFilter.ts
import {
  Catch,
  HttpException as HttpException2,
  HttpStatus as HttpStatus2
} from "@nestjs/common";
import { MongoServerError } from "mongodb";
import { Logger } from "@nestjs/common";
var AllHttpExceptionsFilter = class {
  constructor() {
    this.logger = new Logger();
  }
  // Logger for logging errors
  // Method to catch and handle exceptions
  catch(exception, host) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();
    switch (exception.constructor) {
      case HttpException2: {
        let httpException = exception;
        const status = httpException.getStatus();
        const { error } = httpException.getResponse();
        const res = {
          isCustom: true,
          statusCode: status,
          error,
          timestamp: (/* @__PURE__ */ new Date()).toISOString(),
          path: request.url
        };
        this.logger.error({ ...res, message: httpException.getResponse() });
        response.status(status).json(res);
        break;
      }
      case MongoServerError: {
        let mongoException = exception;
        switch (mongoException.code) {
          case 11e3: {
            const error = {
              code: mongoException.code + `${Object.keys(mongoException.keyValue)}`,
              message: `Duplicate unique key '${Object.keys(
                mongoException.keyValue
              )}'`
            };
            const res = {
              isCustom: true,
              statusCode: HttpStatus2.BAD_REQUEST,
              error,
              timestamp: (/* @__PURE__ */ new Date()).toISOString(),
              path: request.url
            };
            this.logger.error({ ...res, message: error });
            response.status(HttpStatus2.BAD_REQUEST).json(res);
            break;
          }
          default:
            this.logger.error(exception);
            response.status(500).json({
              isCustom: false,
              statusCode: 500,
              error: {
                code: 50003,
                message: "Failed to do something async with an unspecified mongo error"
              },
              exception,
              timestamp: (/* @__PURE__ */ new Date()).toISOString(),
              path: request.url
            });
        }
        break;
      }
      default: {
        switch (true) {
          case exception.constructor?.toString().includes("class MongoServerError extends MongoError"): {
            const mongoException = exception;
            const error = {
              code: mongoException.code + `${Object.keys(mongoException.keyValue)}`,
              message: `Duplicate unique key '${Object.keys(
                mongoException.keyValue
              )}'`
            };
            const res = {
              isCustom: true,
              statusCode: HttpStatus2.BAD_REQUEST,
              error,
              timestamp: (/* @__PURE__ */ new Date()).toISOString(),
              path: request.url
            };
            this.logger.error({ ...res, message: error });
            response.status(HttpStatus2.BAD_REQUEST).json(res);
            break;
          }
          default: {
            this.logger.error(exception);
            const { message } = exception;
            response.status(500).json({
              isCustom: false,
              statusCode: 500,
              error: {
                code: 500004,
                message
              },
              timestamp: (/* @__PURE__ */ new Date()).toISOString(),
              path: request.url
            });
          }
        }
      }
    }
  }
};
AllHttpExceptionsFilter = __decorateClass([
  Catch()
], AllHttpExceptionsFilter);

// src/winston/index.ts
import { WinstonModule } from "nest-winston";
import * as winstonLib from "winston";
var winston = WinstonModule.forRoot({
  level: "error",
  // Set log level to 'error'
  format: winstonLib.format.combine(
    winstonLib.format.json(),
    // Format logs as JSON
    winstonLib.format.timestamp(),
    // Add timestamps to logs
    winstonLib.format.prettyPrint()
    // Pretty print the logs for better readability
  ),
  transports: [
    new winstonLib.transports.Console(),
    // Output logs to the console
    new winstonLib.transports.File({
      filename: "./Logs/system_error.log",
      // Log errors to a file
      level: "error"
      // Set log level for this transport to 'error'
    })
  ]
});

// src/redis/redis.module.ts
import * as redisStore from "cache-manager-redis-store";
import { CacheModule } from "@nestjs/cache-manager";

// src/redis/redis.config.ts
import * as dotenv2 from "dotenv";
dotenv2.config();
var redisConfig = {
  dbName: process.env.REDIS_NAME,
  // Redis database name
  dbHost: process.env.REDIS_HOST,
  // Host where Redis is running
  dbPort: process.env.REDIS_PORT,
  // Port on which Redis is listening
  dbPassword: process.env.REDIS_PASSWORD,
  // Password for Redis authentication
  dbIndex: process.env.REDIS_DATABASE_INDEX
  // Index of the Redis database
};

// src/redis/redis.module.ts
var RedisConnection = CacheModule.register({
  isGlobal: true,
  // Make the cache module global
  store: redisStore,
  // Use Redis store for caching
  url: `redis://:${redisConfig.dbPassword}@${redisConfig.dbHost}:${redisConfig.dbPort}/${redisConfig.dbIndex}`
  // Construct Redis URL from config
});

// src/passport/jwt/jwt.module.ts
import { JwtModule as JwtModuleBase } from "@nestjs/jwt";

// src/passport/jwt/jwt.config.ts
import * as dotenv3 from "dotenv";
dotenv3.config();
var jwtConfig = {
  accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
  // Secret key for JWT access tokens
  expiresInJwt: process.env.EXPIRES_IN_JWT,
  // Expiration time for JWT tokens
  issuer: process.env.ISSUER
  // Issuer of the JWT tokens
};

// src/passport/jwt/jwt.module.ts
var JwtModule = JwtModuleBase.register({
  secret: jwtConfig.accessTokenSecret,
  // Secret key for signing JWT tokens
  signOptions: { expiresIn: jwtConfig.expiresInJwt }
  // Expiration time for JWT tokens
});

// src/passport/jwt/jwt.strategy.ts
import { ExtractJwt, Strategy } from "passport-jwt";
import { PassportStrategy } from "@nestjs/passport";
import { HttpException as HttpException3, HttpStatus as HttpStatus3, Injectable } from "@nestjs/common";

// src/passport/jwt/jwt.errors.ts
var passportErrors = {
  accessTokenNotExist: {
    code: 50005,
    message: "Header does not contain access token."
    // Error message for missing access token in header
  },
  invalidAccessToken: {
    code: 50006,
    message: "Invalid access token."
    // Error message for invalid access token
  },
  ExpiredAccessToken: {
    code: 50007,
    message: "Access token Expired"
    // Error message for expired access token
  }
};

// src/passport/jwt/jwt.strategy.ts
import * as jwt from "jsonwebtoken";

// src/passport/constant/index.ts
var passportStrategy = {
  local: "local",
  // Local strategy
  localMobile: "localMobile",
  // Local strategy for mobile
  otp: "otp",
  // OTP strategy
  jwt: "jwt"
  // JWT strategy
};

// src/passport/jwt/jwt.strategy.ts
var JwtStrategy = class extends PassportStrategy(
  Strategy,
  passportStrategy.jwt
) {
  constructor(redisService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      // Extract JWT from authorization header
      ignoreExpiration: false,
      // Do not ignore token expiration
      secretOrKey: jwtConfig.accessTokenSecret,
      // Secret key for JWT validation
      passReqToCallback: true
      // Pass the request to the callback function
    });
    this.redisService = redisService;
  }
  // Validate method to check the JWT payload and retrieve user privileges from Redis
  async validate(req, payload) {
    const [authToken, privileges] = await Promise.all([
      this.redisService.get({ key: `${payload.account}` }),
      // Get auth token from Redis
      this.redisService.get({ key: `${payload.account}privileges` })
      // Get user privileges from Redis
    ]);
    if (!authToken || !privileges)
      throw new HttpException3(
        { error: passportErrors.ExpiredAccessToken },
        HttpStatus3.UNAUTHORIZED
      );
    const redisPayload = jwt.decode(authToken);
    return { ...redisPayload, privileges };
  }
};
JwtStrategy = __decorateClass([
  Injectable()
], JwtStrategy);

// src/passport/jwt/jwt.guard.ts
import {
  Injectable as Injectable2,
  HttpException as HttpException4,
  HttpStatus as HttpStatus4
} from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
var JwtAuthGuard = class extends AuthGuard(passportStrategy.jwt) {
  // Override canActivate method to add custom logic before default guard activation
  canActivate(context) {
    const request = context.switchToHttp().getRequest();
    if (request.headers["authorization"]?.split(" ")[1] === "null")
      throw new HttpException4(
        { error: passportErrors.accessTokenNotExist },
        HttpStatus4.UNAUTHORIZED
      );
    return super.canActivate(context);
  }
  // Override handleRequest method to handle errors and user authentication
  handleRequest(err, user, info) {
    if (err || !user) {
      throw err || new HttpException4(
        { error: passportErrors.accessTokenNotExist },
        HttpStatus4.UNAUTHORIZED
      );
    }
    return user;
  }
};
JwtAuthGuard = __decorateClass([
  Injectable2()
], JwtAuthGuard);

// src/validation/joi.pips.ts
import {
  Injectable as Injectable3,
  HttpException as HttpException5,
  HttpStatus as HttpStatus5
} from "@nestjs/common";
var JoiValidationPipe = class {
  // Constructor to initialize the schema and unknown property
  constructor(schema, unknown = false) {
    this.schema = schema;
    this.unknown = unknown;
  }
  // Method to transform and validate the incoming value using Joi schema
  transform(value, metadata) {
    const { error } = this.schema.unknown(this.unknown).validate(value, { abortEarly: false });
    if (error) {
      throw new HttpException5(
        {
          error: {
            code: 5e4,
            message: error.message.replace(/(\"|\[|\d\])/g, "")
            // Format the error message
            // message : 'Validation failed'
            // message: error?.details.flatMap((val) => {
            //   return {
            //     message: val.message.split('"').join(''),
            //     path: val.context.label,
            //   };
            // }),
          }
        },
        HttpStatus5.BAD_REQUEST
      );
    }
    return value;
  }
};
JoiValidationPipe = __decorateClass([
  Injectable3()
], JoiValidationPipe);

// src/validation/schemas/index.ts
var schemas_exports2 = {};
__export(schemas_exports2, {
  alpha: () => alpha,
  languageObject: () => languageObject,
  localizableString: () => localizableString,
  mongoId: () => mongoId,
  parseStringRule: () => parseStringRule,
  password: () => password,
  stringNumber: () => stringNumber,
  stringRole: () => stringRole
});

// src/validation/schemas/localizableString.validation.ts
import * as joi from "joi";

// src/config/index.ts
import * as dotenv4 from "dotenv";
dotenv4.config();
var serverConfig = {
  // Network configuration for the server
  host: process.env.SERVER_HOST,
  // The hostname or IP address on which the server runs
  port: process.env.SERVER_PORT,
  // The port on which the server listens
  // Environment setting for the Node.js server
  nodeEnv: process.env.NODE_ENV,
  // Environment type (development, production, etc.)
  // API specific configurations
  apIVersion: process.env.API_VERSION,
  // Version of the API
  globalPrefix: process.env.GLOBAL_PREFIX,
  // Global URL prefix for all routes
  // Swagger API documentation credentials
  swaggerUsername: process.env.SWAGGER_USERNAME,
  // Username for Swagger UI authentication
  swaggerPassword: process.env.SWAGGER_PASSWORD,
  // Password for Swagger UI authentication
  // Default settings for application behavior
  defaultLanguage: process.env.DEFAULT_LANGUAGE,
  // Default language setting for localization
  defaultPrice: process.env.DEFAULT_PRICE,
  // Default price used throughout the application
  // File handling configurations
  mainLocalFileUploadDestination: process.env.MAIN_LOCAL_FILE_UPLOAD_DESTINATION,
  // Local directory path where files are uploaded
  storageUploadLostFile: process.env.STORAGE_UPLOADED_LOST_FILES,
  // Directory for files that fail during upload
  fileUrlPrefix: process.env.FILE_URL_PREFIX,
  // URL prefix for accessing uploaded files
  fileSaveBasePath: process.env.FILE_SAVE_BASE_PATH,
  // Base file path for saving files
  // URL configuration
  serverUrl: process.env.SERVER_URL,
  // Full URL of the server
  // Security and access configurations
  otpValidTime: process.env.OTP_VALID_TIME,
  // Validity time for OTP (One Time Password)
  tokenTTL: +process.env.TOKEN_TTL
  // Time to live for authentication tokens, cast to number
};

// src/validation/schemas/localizableString.validation.ts
var localizableString = () => {
  const languageKey = {
    en: joi.string().allow("", null),
    // Allow empty string or null for English
    ar: joi.string().allow("", null)
    // Require string for Arabic
  };
  if (serverConfig.defaultLanguage)
    languageKey[`${serverConfig.defaultLanguage}`] = joi.string().required();
  return joi.object(languageKey);
};

// src/validation/schemas/mongoId.validation.ts
import * as joi2 from "joi";
var mongoId = () => {
  return joi2.string().regex(/^[a-fA-F0-9]{24}$/, "mongo Id valid");
};

// src/validation/schemas/password.validation.ts
import * as joi3 from "joi";
var password = () => {
  return joi3.string().regex(
    /^[a-zA-Z\d\s~`!@#\$%\^&\*\(\)_\-\+={}\[\]\|:;"'<>,.\?\/\\]+$/i,
    // Regex pattern for valid password characters
    "password valid"
    // Custom error message for regex validation
  );
};

// src/validation/schemas/stringNumber.validation.ts
import * as joi4 from "joi";
var stringNumber = () => {
  return joi4.string().regex(/^\d+$/, "number valid");
};

// src/validation/schemas/languageObject.validation.ts
import * as joi5 from "joi";
var languageCodes = [
  "aa",
  "ab",
  "af",
  "ak",
  "am",
  "ar",
  "as",
  "av",
  "ay",
  "az",
  "ba",
  "be",
  "bg",
  "bh",
  "bi",
  "bm",
  "bn",
  "bo",
  "br",
  "bs",
  "ca",
  "ce",
  "ch",
  "co",
  "cr",
  "cs",
  "cu",
  "cv",
  "cy",
  "da",
  "de",
  "dv",
  "dz",
  "ee",
  "el",
  "en",
  "eo",
  "es",
  "et",
  "eu",
  "fa",
  "ff",
  "fi",
  "fj",
  "fo",
  "fr",
  "fy",
  "ga",
  "gd",
  "gl",
  "gn",
  "gu",
  "gv",
  "ha",
  "he",
  "hi",
  "ho",
  "hr",
  "ht",
  "hu",
  "hy",
  "hz",
  "ia",
  "id",
  "ie",
  "ig",
  "ii",
  "ik",
  "io",
  "is",
  "it",
  "iu",
  "ja",
  "jv",
  "ka",
  "kg",
  "ki",
  "kj",
  "kk",
  "kl",
  "km",
  "kn",
  "ko",
  "kr",
  "ks",
  "ku",
  "kv",
  "kw",
  "ky",
  "la",
  "lb",
  "lg",
  "li",
  "ln",
  "lo",
  "lt",
  "lu",
  "lv",
  "mg",
  "mh",
  "mi",
  "mk",
  "ml",
  "mn",
  "mr",
  "ms",
  "mt",
  "my",
  "na",
  "nb",
  "nd",
  "ne",
  "ng",
  "nl",
  "nn",
  "no",
  "nr",
  "nv",
  "ny",
  "oc",
  "oj",
  "om",
  "or",
  "os",
  "pa",
  "pi",
  "pl",
  "ps",
  "pt",
  "qu",
  "rm",
  "rn",
  "ro",
  "ru",
  "rw",
  "sa",
  "sc",
  "sd",
  "se",
  "sg",
  "si",
  "sk",
  "sl",
  "sm",
  "sn",
  "so",
  "sq",
  "sr",
  "ss",
  "st",
  "su",
  "sv",
  "sw",
  "ta",
  "te",
  "tg",
  "th",
  "ti",
  "tk",
  "tl",
  "tn",
  "to",
  "tr",
  "ts",
  "tt",
  "tw",
  "ty",
  "ug",
  "uk",
  "ur",
  "uz",
  "ve",
  "vi",
  "vo",
  "wa",
  "wo",
  "xh",
  "yi",
  "yo",
  "za",
  "zh",
  "zu"
];
var languageObject = () => {
  return joi5.object().pattern(
    joi5.string().valid(...languageCodes),
    // Validate keys as valid language codes
    joi5.string().trim().min(1).required()
    // Validate values as non-empty strings
  ).min(1);
};

// src/validation/schemas/parseStringRule.validation.ts
import { HttpException as HttpException6, HttpStatus as HttpStatus6 } from "@nestjs/common";
import * as joi7 from "joi";

// src/validation/schemas/alpha.validation.ts
import * as joi6 from "joi";
var alpha = () => {
  return joi6.string().regex(/^[a-zA-Z\u0621-\u064A\s]+$/, "alpha valid");
};

// src/validation/schemas/parseStringRule.validation.ts
var parseStringRule = (rule, valueRequired = false) => {
  const validData = [];
  let baseRule = null;
  if (rule)
    rule.split("||").map((option) => {
      if (option.startsWith("'") && option.endsWith("'")) {
        validData.push(option.slice(1, -1));
        return;
      }
      const subrules = option.split("&&");
      subrules.forEach((subrule) => {
        if (subrule.startsWith("'") && subrule.endsWith("'")) {
          validData.push(subrule.slice(1, -1));
          return;
        }
        const [rule2, param] = subrule.split(":");
        switch (rule2) {
          case "string":
            baseRule = baseRule ? baseRule.string() : joi7.string();
            break;
          case "min":
            baseRule = baseRule ? baseRule.min(parseInt(param)) : joi7.string().min(parseInt(param));
            break;
          case "alpha":
            baseRule = baseRule ? baseRule.alpha() : alpha();
            break;
          case "max":
            baseRule = baseRule ? baseRule.max(parseInt(param)) : joi7.string().max(parseInt(param));
            break;
          case "integer":
            baseRule = baseRule ? baseRule.integer() : joi7.number().integer();
            break;
          case "number":
            baseRule = baseRule ? baseRule.number() : joi7.number();
            break;
          case "email":
            baseRule = baseRule ? baseRule.email() : joi7.string().email();
            break;
          case "ip":
            baseRule = baseRule ? baseRule.ip() : joi7.string().ip();
            break;
          case "boolean":
            baseRule = baseRule ? baseRule.boolean() : joi7.boolean();
            break;
          case "":
            baseRule = joi7.any();
            break;
          case "regex":
            baseRule = baseRule ? baseRule.pattern(new RegExp(param)) : joi7.string().pattern(new RegExp(param));
            break;
          case "password":
            baseRule = baseRule ? baseRule.password().min(8) : password().min(8);
            break;
          case "json":
            baseRule = baseRule ? baseRule.custom((value) => {
              try {
                JSON.parse(value);
                return value;
              } catch {
                throw new HttpException6(
                  {
                    error: {
                      code: 5e4,
                      message: "Invalid JSON"
                    }
                  },
                  HttpStatus6.BAD_REQUEST
                );
              }
            }) : joi7.custom((value) => {
              try {
                JSON.parse(value);
                return value;
              } catch {
                throw new HttpException6(
                  {
                    error: {
                      code: 5e4,
                      message: "Invalid JSON"
                    }
                  },
                  HttpStatus6.BAD_REQUEST
                );
              }
            });
            break;
          default: {
            throw new HttpException6(
              {
                error: {
                  code: 5e4,
                  message: `Unsupported validation type: ${rule2}`
                }
              },
              HttpStatus6.BAD_REQUEST
            );
          }
        }
      });
      return;
    });
  if (validData.length) {
    baseRule = baseRule ? baseRule.valid(...validData) : joi7.string().valid(...validData);
  }
  if (baseRule === null) {
    baseRule = joi7.any();
  }
  if (valueRequired) {
    baseRule = baseRule.required();
  }
  return baseRule;
};

// src/validation/schemas/stringRole.validation.ts
import * as joi8 from "joi";
var stringRole = () => {
  return joi8.string().regex(
    /^(?:(?:\s*('(?:[^'\\]|\\.)+'(\s*\|\|\s*'(?:[^'\\]|\\.)+')*|string(\s*&&\s*(min|max):\d+)*|alpha|integer|email|ip|password|json|required|number|regex:[^'\\]+)(\s*&&\s*('(?:[^'\\]|\\.)+'(\s*\|\|\s*'(?:[^'\\]|\\.)+')*|string|min:\d+|max:\d+|alpha|integer|email|ip|password|json|required|number|regex:[^'\\]+))*\s*))$/,
    "validation schema valid"
    // Custom error message for regex validation
  );
};

// src/authorization/policies/policies.guard.ts
import {
  Injectable as Injectable4,
  HttpException as HttpException7,
  HttpStatus as HttpStatus7
} from "@nestjs/common";

// src/authorization/policies/policies.decorator.ts
import { SetMetadata } from "@nestjs/common";
var CHECK_POLICIES_KEY = "policies";
var Policies = (keys, values) => SetMetadata(CHECK_POLICIES_KEY, { keys, values });

// src/authorization/policies/policies.errors.ts
var policyError = {
  // 'notAllowed' error configuration for unauthorized actions.
  notAllowed: {
    code: 403,
    // HTTP status code for Forbidden
    message: "You are not allowed to perform this action."
  }
};

// src/authorization/policies/policies.guard.ts
var PoliciesGuard = class {
  constructor(reflector) {
    this.reflector = reflector;
  }
  canActivate(context) {
    const policyHandlers = this.reflector.get(CHECK_POLICIES_KEY, context.getHandler());
    const { user } = context.switchToHttp().getRequest();
    if (!user || !user.privileges) {
      throw new HttpException7(
        { error: policyError.notAllowed },
        HttpStatus7.FORBIDDEN
      );
    }
    if (!policyHandlers.keys.some((key) => user.privileges[key])) {
      throw new HttpException7(
        { error: policyError.notAllowed },
        HttpStatus7.FORBIDDEN
      );
    }
    const privilegeKeys = policyHandlers.keys.filter(
      (key) => user.privileges[key]
    );
    if (!privilegeKeys.some(
      (key) => policyHandlers.values.some((value) => user.privileges[key][value])
    )) {
      throw new HttpException7(
        { error: policyError.notAllowed },
        HttpStatus7.FORBIDDEN
      );
    }
    return true;
  }
};
PoliciesGuard = __decorateClass([
  Injectable4()
], PoliciesGuard);

// src/authorization/userTypes/userTypes.guard.ts
import {
  Injectable as Injectable5,
  HttpException as HttpException8,
  HttpStatus as HttpStatus8
} from "@nestjs/common";

// src/authorization/userTypes/userTypes.decorator.ts
import { SetMetadata as SetMetadata2 } from "@nestjs/common";
var CHECK_TYPES_KEY = "userType";
var UserTypesMetadata = (values) => SetMetadata2(CHECK_TYPES_KEY, { values });

// src/authorization/userTypes/userTypes.errors.ts
var userTypeError = {
  /**
   * Generates a custom error object for user type mismatches.
   * @param {string} type The expected user type that was not met.
   * @returns {object} An error object containing the error code and a descriptive message.
   */
  wrongType: (type) => {
    return {
      code: 50002,
      // Custom application-specific error code.
      message: `User is not a ${type}`
      // Dynamic error message indicating the mismatch.
    };
  }
};

// src/authorization/userTypes/userTypes.guard.ts
var UserTypesGuard = class {
  constructor(reflector) {
    this.reflector = reflector;
  }
  /**
   * Determines if an incoming request should be allowed based on the user's type.
   * Uses metadata defined by the `UserTypesMetadata` decorator to restrict access.
   *
   * @param context The execution context providing details and utilities for handling the current request.
   * @returns true if the user meets the required type, otherwise throws an HttpException.
   */
  canActivate(context) {
    const typeHandlers = this.reflector.get(CHECK_TYPES_KEY, context.getHandler());
    const { user } = context.switchToHttp().getRequest();
    if (!typeHandlers || !typeHandlers.values.includes(user.type)) {
      throw new HttpException8(
        { error: userTypeError.wrongType(user.type) },
        HttpStatus8.FORBIDDEN
      );
    }
    return true;
  }
};
UserTypesGuard = __decorateClass([
  Injectable5()
], UserTypesGuard);

// src/pagination/pagination.constant.ts
var paginationConstant = {
  total: false,
  // Default value indicating if total count is needed
  needPagination: true,
  // Default value indicating if pagination is needed
  page: 0,
  // Default page number
  limit: {
    // Default and maximum limits for items per page
    default: 50,
    // Default items per page
    max: 100
    // Maximum items per page
  },
  sort: { _id: -1 }
  // Default sorting order
};

// src/pagination/pagination.joi.ts
import * as joi9 from "joi";
var joiPagination = (needPaginationValidValues) => {
  return {
    total: joi9.boolean().default(paginationConstant.total),
    // Validate and set default for total
    needPagination: joi9.boolean().required().valid(...needPaginationValidValues),
    // Validate needPagination with allowed values
    page: joi9.number().min(0).default(paginationConstant.page),
    // Validate and set default for page
    limit: joi9.number().min(0).max(paginationConstant.limit.max).default(paginationConstant.limit.default)
    // Validate and set default for limit
  };
};

// src/pagination/pagination.parser.ts
var _ = __require("lodash");
var paginationParser = (reqQuery) => {
  const fields = ["total", "page", "limit", "needPagination"];
  const pagination = _.pick(reqQuery, fields);
  const page = +pagination.page || 0;
  pagination.total = pagination.total === "true" || pagination.total === true ? true : paginationConstant.total;
  pagination.limit = +pagination.limit || paginationConstant.limit.default;
  pagination.skip = page * pagination.limit;
  pagination.needPagination = pagination.needPagination === "false" || pagination.needPagination === false ? false : paginationConstant.needPagination;
  const criteria = _.omit(reqQuery, fields);
  return { pagination, criteria };
};

// src/pagination/pagination-request.dto.ts
var PaginationRequest = class {
  // Number of items per page
};

// src/decorators/decoratorComposition/custom/controller.decorator.ts
import { Controller, applyDecorators } from "@nestjs/common";
import { UseGuards } from "@nestjs/common";
function AuthenticatedController({
  controller
}) {
  return applyDecorators(UseGuards(JwtAuthGuard), Controller(controller));
}
function CustomController({
  guard,
  controller
}) {
  return applyDecorators(UseGuards(guard), Controller(controller));
}

// src/decorators/decoratorComposition/custom/authorized-api-method.decorator.ts
import { applyDecorators as applyDecorators3 } from "@nestjs/common";

// src/decorators/decoratorComposition/custom/enum.ts
var APIMETHODS = /* @__PURE__ */ ((APIMETHODS2) => {
  APIMETHODS2["GET"] = "GET";
  APIMETHODS2["POST"] = "POST";
  APIMETHODS2["PATCH"] = "PATCH";
  APIMETHODS2["PUT"] = "PUT";
  APIMETHODS2["DELETE"] = "DELETE";
  return APIMETHODS2;
})(APIMETHODS || {});

// src/decorators/decoratorComposition/custom/authorization.decorator.ts
import { applyDecorators as applyDecorators2 } from "@nestjs/common";
import { UseGuards as UseGuards2 } from "@nestjs/common";
function Authorization({
  privilegeKeys,
  policyAccessMode = []
}) {
  return applyDecorators2(
    UseGuards2(PoliciesGuard),
    // Use PoliciesGuard for authorization
    Policies(privilegeKeys, policyAccessMode)
    // Apply Policies decorator with privilege keys and access modes
  );
}

// src/decorators/decoratorComposition/custom/authorized-api-method.decorator.ts
import { Delete, Get, Patch, Post, Put } from "@nestjs/common";
var getMethod = ({
  method,
  apiUrl
}) => {
  const methodMap = {
    ["GET" /* GET */]: Get(apiUrl),
    ["POST" /* POST */]: Post(apiUrl),
    ["PATCH" /* PATCH */]: Patch(apiUrl),
    ["PUT" /* PUT */]: Put(apiUrl),
    ["DELETE" /* DELETE */]: Delete(apiUrl)
  };
  return methodMap[method];
};
function AuthorizedApiMethod({
  apiUrl,
  method,
  privilegeKeys,
  policyAccessMode = []
}) {
  return applyDecorators3(
    applyDecorators3(
      Authorization({ privilegeKeys, policyAccessMode }),
      // Apply authorization
      getMethod({ apiUrl, method })
      // Apply the method decorator
    )
  );
}

// src/decorators/decoratorComposition/custom/user.types.decorator.ts
import { applyDecorators as applyDecorators4 } from "@nestjs/common";
import { UseGuards as UseGuards3 } from "@nestjs/common";
function UserType(...values) {
  return applyDecorators4(
    UserTypesMetadata(values),
    // Apply user types metadata
    UseGuards3(UserTypesGuard)
    // Apply user types guard
  );
}

// src/decorators/paramDecorators/user.decorators.ts
import { createParamDecorator } from "@nestjs/common";
var User = createParamDecorator(
  (data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  }
);
var UserContent = createParamDecorator(
  (data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const user = request.user;
    return data ? user?.[data] : user;
  }
);

// src/decorators/paramDecorators/header.decorators.ts
import { createParamDecorator as createParamDecorator2 } from "@nestjs/common";
var Headers = createParamDecorator2(
  (data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    request.headers.languageKey = request.headers["accept-language"] || serverConfig.defaultLanguage;
    request.headers.priceKey = serverConfig.defaultPrice;
    request.headers.storeView = request.headers?.storeview;
    return request.headers;
  }
);
var HeadersContent = createParamDecorator2(
  (data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    const headers = request.headers;
    return data ? headers?.[data] : headers;
  }
);

// src/services/localization/index.ts
import { Injectable as Injectable6 } from "@nestjs/common";
var LocalizationService = class {
  // Method to get the price based on the provided price key or default price key
  price({ obj, priceKey }) {
    return obj[priceKey] || obj[serverConfig.defaultPrice];
  }
  // Method to get the localized string based on the provided language key or default language key
  string({ obj, languageKey }) {
    return obj[languageKey] || obj[serverConfig.defaultLanguage];
  }
};
LocalizationService = __decorateClass([
  Injectable6()
], LocalizationService);

// src/services/fileUrl/index.ts
import { Injectable as Injectable7 } from "@nestjs/common";
var FileUrlService = class {
  // Method to generate the full URL for a given file
  generate({
    filename,
    storageFilePath
  }) {
    return serverConfig.serverUrl + // Base server URL
    serverConfig.fileUrlPrefix + // Prefix for file URLs
    storageFilePath + // Path where the file is stored
    filename;
  }
};
FileUrlService = __decorateClass([
  Injectable7()
], FileUrlService);

// src/services/redis/index.ts
import { Injectable as Injectable8, Inject } from "@nestjs/common";
import { CACHE_MANAGER } from "@nestjs/common/cache";
var RedisService = class {
  // Constructor to inject the cache manager
  constructor(cacheManager) {
    this.cacheManager = cacheManager;
  }
  // Method to get a value from Redis by key
  async get({ key }) {
    return await this.cacheManager.get(key);
  }
  // Method to set a value in Redis with an optional TTL
  async set({
    key,
    value,
    ttl
  }) {
    return await this.cacheManager.set(key, value, { ttl });
  }
  // Method to delete a value from Redis by key
  async del({ key }) {
    return await this.cacheManager.del(key);
  }
};
RedisService = __decorateClass([
  Injectable8(),
  __decorateParam(0, Inject(CACHE_MANAGER))
], RedisService);

// src/services/privilege/index.ts
import { Injectable as Injectable9 } from "@nestjs/common";
var CreatePrivilegeService = class {
  // Static method to create a privilege string based on the HTTP method and feature name
  static create(method, featureName) {
    return CreatePrivilegeService.httpMethod[method] + featureName;
  }
};
// Mapping of HTTP methods to privilege prefixes
CreatePrivilegeService.httpMethod = {
  post: "post_",
  get: "get_",
  patch: "patch_",
  delete: "delete_"
};
CreatePrivilegeService = __decorateClass([
  Injectable9()
], CreatePrivilegeService);

// src/services/functions/sort/index.ts
var getSortFormat = (sortKey) => sortKey.startsWith("-") ? { [`${sortKey.substring(1)}`]: -1 } : { [`${sortKey}`]: 1 };

// src/services/interceptors/after-request-interceptor.ts
import {
  Injectable as Injectable10
} from "@nestjs/common";
import { tap } from "rxjs/operators";
var RefreshTokenTTLInterceptor = class {
  constructor(redisService) {
    this.redisService = redisService;
  }
  // Intercept method to handle the request and response
  intercept(context, next) {
    return next.handle().pipe(
      tap(() => {
        const request = context.switchToHttp().getRequest();
        if (request?.user?.account && request?.headers?.authorization) {
          const user = request.user;
          this.redisService.get({
            key: user.account.toString()
          }).then((authToken) => {
            const queries = [];
            if (authToken)
              queries.push(
                this.redisService.set({
                  key: user.account.toString(),
                  value: authToken,
                  ttl: serverConfig.tokenTTL
                  // Set TTL for the auth token
                })
              );
            if (user.privileges)
              queries.push(
                this.redisService.set({
                  key: `${user.account.toString()}privileges`,
                  value: user.privileges,
                  ttl: serverConfig.tokenTTL
                  // Set TTL for the privileges
                })
              );
            Promise.all(queries);
          });
        }
      })
    );
  }
};
RefreshTokenTTLInterceptor = __decorateClass([
  Injectable10()
], RefreshTokenTTLInterceptor);

// src/dto/response/index.ts
var ResponseWrapper = class {
  // Constructor to initialize the response wrapper with data
  constructor(data) {
    return data;
  }
};
export {
  APIMETHODS,
  AllHttpExceptionsFilter,
  AuthenticatedController,
  AuthorizedApiMethod,
  BaseMongoRepository,
  CreatePrivilegeService,
  CustomController,
  FileUrlService,
  Headers,
  HeadersContent,
  JoiValidationPipe,
  JwtAuthGuard,
  JwtModule,
  JwtStrategy,
  LocalizationService,
  MongoDbConnection,
  PaginationRequest,
  Policies,
  PoliciesGuard,
  RedisConnection,
  RedisService,
  RefreshTokenTTLInterceptor,
  ResponseWrapper,
  User,
  UserContent,
  UserType,
  UserTypesGuard,
  UserTypesMetadata,
  getSortFormat,
  joiPagination,
  jwtConfig,
  mongoDbConfig,
  mongoDbUrl,
  schemas_exports as mongodbSchemas,
  paginationConstant,
  paginationParser,
  passportStrategy,
  serverConfig,
  schemas_exports2 as validationSchemas,
  winston
};
